﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Execution;
using Microsoft.Build.Framework;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Configuration;

namespace DeploymentConsole
{
    class Program
    {
        /*
         * Syntax: deploymentconsole.exe [repo name] [detailed console]
         * 
         * Example: deploymentconsole.exe VEK-649-post-script-for-ci-builder T
         * 
         * Description:         What it tries to do:
         *                      1. download repo using Git Clone (Assumes Git is logged in!)
         *                      2. run NuGet.exe to restore any NuGet Packages that may be required
         *                      3. run MSBuild to compile project release
         *                      4. create script file for InnoScript
         *                      5. build setup file
         *                      
         *                      NOTES:  any repo will be copied to C:\VEKioskRepos\<repo name>
         *                              a log file will be created as C:\VEKioskRepos\<repo name>_date_time_log.txt
         *                              the setup file will be created as C:\FTP\VEKioskReleases\<repo name>\VEKioskSetup_date_time.exe
         *                              
         *Parameters:
         *  [repo name]         should be the name of the Git repo
         *  
         *  [detailed console]  set to "T" or "True" to view all activity in the console
         *  
         *Requirements
         *                      There are supporting directories that must be present
         *                          1. C:\VEKioskStaging - Any files you wish to be included in the C:\VEKioskUI install folder, place here
         *                          2. C:\VEKioskStaging\Icons - Icons required for file links
         *                              a. The icons required to be present in this directory are "KioskUI.ico", "Setup.ico", and "Test.ico".
         *                                 As long as these files exist they can be whatever images you like.
         *                      
         *                      There are supporting applications that must be installed and their directories defined
         *                          1. Git - Default path is "C:\Program Files\Git\bin\git.exe"
         *                          2. Nuget - Default path is ""C:\VEKioskUIStaging\NuGet\nuget.exe""
         *                          3. MSBuild - Default path is "C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe"
         *                          4. InnoScript - Default path is "C:\Program Files (x86)\Inno Setup 5\ISCC.exe"
         *                          
         *                      NOTE: These directory paths are availiable in the application configuration file
         * */

        // Source directories
        static string KioskRelease = @"\Projects\VEII Kiosk UI\KioskUI\KioskUI\bin\Release";
        static string KioskImages = @"\Projects\VEII Kiosk UI\KioskUI\KioskUI\Images";
        static string SetupRelease = @"\Projects\VEII Kiosk UI\KioskUI\KioskConfigurationUtility\bin\Release";
        static string TesterRelease = @"\Projects\VEII Kiosk UI\KioskUI\TestSuite\bin\Release";
        static string ShellRelease = @"\Projects\VEII Kiosk UI\KioskUI\VEKioskShell\VEKioskShell\bin\Release";

        // Installation Directories
        static string IconInstallPath = @"Icons";
        static string KioskReleaseInstallPath = @"VEKiosk\bin\Release";
        static string KioskImagesInstallPath = @"VEKiosk\Images";
        static string SetupInstallPath = @"Setup";
        static string TesterInstallPath = @"Tester";
        static string ShellInstallPath = @"VEKioskShell";

        // Executable Names
        static string KioskExe = @"KioskUI.exe";
        static string SetupEXE = @"KioskConfigurationUtility.exe";
        static string TesterExe = @"TestSuite.exe";

        // Icon Display Names
        static string KioskExeName = "VE Kiosk UI";
        static string SetupExeName = "Setup Utility";
        static string TesterExeName = "Test Utility";

        // Variables
        static string KioskUIVersion = string.Empty;
        static string RepoName = string.Empty;
        static string InstallerPath = string.Empty;
        static string RepoPath = string.Empty;
        static string ProjectFile = string.Empty;
        static string result = string.Empty;
        static string log = string.Empty;
        static string newlines = $"{Environment.NewLine}{Environment.NewLine}";
        static string msg = string.Empty;
        static string BuildDate = string.Empty;
        static bool detailed = false;
        static Dictionary<string, string> configItems;

        static void Main(string[] args)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings as NameValueCollection;
                configItems = new Dictionary<string, string>();

                msg = $"Loading Application Config Items . . .{newlines}";
                log = msg;

                SetConfigItems(appSettings);

                if (appSettings == null)
                {
                    var message = "There was an error reading the application configuration";
                    Console.WriteLine(message);
                    ExitApplication(-1, message, RepoPath);
                }

                var now = DateTime.Now;
                BuildDate = $"{now.Month}-{now.Day}-{now.Year}_{now.Hour}{now.Minute}{now.Second}";
                Dictionary<int, string> argList = new Dictionary<int, string>();
                var index = 0;

                foreach(var x in args)
                {
                    argList.Add(index++, x);
                }

                if(argList.ContainsKey(0)) // Repo Name Parameter
                {
                    RepoName = argList.Where(x => x.Key == 0).FirstOrDefault().Value;
                }

                if(argList.ContainsKey(1)) // Detailed Output Parameter
                {
                    var res = argList.Where(x => x.Key == 0).FirstOrDefault().Value;
                    if(argList.Where(x => x.Key == 0).FirstOrDefault().Value.ToUpper().Contains("T"))
                    {
                        detailed = true;
                    }
                }

                if(string.IsNullOrEmpty(RepoName))
                {
#if DEBUG
                    Console.WriteLine("Please enter repo name:");
                    RepoName = Console.ReadLine();
                    if (string.IsNullOrEmpty(RepoName)) Environment.Exit(-1);
#else
                    Console.WriteLine("Please enter a valid repo name");
                    Environment.Exit(-1);
#endif
                }

                InstallerPath = @"C:\ftp\VEKioskReleases\" + RepoName;
                RepoPath = @"C:\VEKioskRepos\" + RepoName;

                msg += $"Creating repo: {RepoName}{newlines}";
                log += msg;
                Console.WriteLine(msg);

                DirectoryInfo dir = new DirectoryInfo(RepoPath);

                if(dir.Exists)
                {
                    Output($"Directory Already Exists!{newlines}Removing . . .{newlines}");

                    try
                    {
                        UpdateFileAttributes(dir);

                        dir.Delete(true);

                        Output($"Success!{newlines}");
                    }
                    catch (Exception ex)
                    {
                        Output(ex.Message);
                        ExitApplication(-1, log, RepoPath);
                    }
                }

                // Download Repo and install to designated path
                result = RunGitClone(RepoName, RepoPath);

                if (result.Contains("fatal"))
                {
                    Output($"{result}{newlines}Exiting . . .");
                    ExitApplication(-1, log, RepoPath);
                }
                else
                {
                    Output($"{result}{newlines}");
                }

                // Before build need to restore any NUGET packages
                Output($"Restoring NuGet packages . . .{newlines}");
                result = RestoreNuGetPackages(RepoPath + @"\Projects\VEII Kiosk UI\KioskUI");

                if (result.Contains("fatal"))
                {
                    Output($"{result}{newlines}Exiting . . .");
                    ExitApplication(-1, log, RepoPath);
                }
                else
                {
                    Output($"{result}{newlines}");
                }

                //string projectFile = @"C:\Nate\Temp\VEKiosk\Projects\VEII Kiosk UI\KioskUI\KioskUI.sln";
                ProjectFile = RepoPath + @"\Projects\VEII Kiosk UI\KioskUI";

                Output($"Attempting to build {RepoName}{newlines}");
                BuildProject(ProjectFile);

                if(log.ToUpper().Contains("FAILED"))
                {
                    Output($"Build Failed{newlines}Exiting . . .");
                    ExitApplication(-1, log, RepoPath);
                }
                else
                {
                    var lookup = "AssemblyVersion: ";
                    KioskUIVersion = log.Substring(log.IndexOf(lookup) + lookup.Length, 14);

                    if (CheckVersion(KioskUIVersion))
                    {
                        Output($"Kiosk UI Version: {KioskUIVersion}{newlines}");
                    }
                    else
                    {
                        Output($"Kiosk UI Version NOT FOUND, setting version to 1.0.0.0{newlines}");
                        KioskUIVersion = "1.0.0.0";
                    }

                    Output($"Build Succeeded{newlines}");
                    Output($"Building Inno Script . . .{newlines}");

                    var script = BuildScript();

                    Output($"Saving Inno Script. . .{newlines}");

                    try
                    {
                        File.WriteAllText(@"C:\Temp\InnoScript.iss", script);
                        Output($"Success{newlines}");
                    }
                    catch (Exception ex)
                    {
                        Output(ex.Message);
                        ExitApplication(-1, log, RepoPath);
                    }

                    Output($"Script saved, now building installer . . .{newlines}");

                    var result = RunInnoSetup(InstallerPath);

                    ExitApplication(1, log, RepoPath);
                }
            }
            catch (Exception ex)
            {
                ExitApplication(-1, ex.Message, RepoPath);
            }
        }

        static void Output(string msg)
        {
            log += msg;
            Console.Write(msg);
        }

        static void SetConfigItems(NameValueCollection list)
        {
            string val = string.Empty;

            foreach (string x in list)
            {
                if (configItems.TryGetValue(x, out val))
                {
                    configItems[x] = list.Get(x);
                }
                else
                {
                    configItems.Add(x, list.Get(x));
                }

                if (detailed)
                {
                    Output($"Loaded {x} path {list.Get(x)} from application config{Environment.NewLine}");
                }
                else
                {
                    log += $"Loaded {x} path {list.Get(x)} from application config{Environment.NewLine}";
                }
            }
        }

        static void UpdateFileAttributes(DirectoryInfo dInfo)
        {
            dInfo.Attributes = FileAttributes.Normal;

            foreach (FileInfo file in dInfo.GetFiles())
            {
                file.Attributes = FileAttributes.Normal;
            }

            foreach (DirectoryInfo subDir in dInfo.GetDirectories())
            {
                UpdateFileAttributes(subDir);
            }
        }

        static bool CheckVersion(string text)
        {
            int val;
            bool result = true;

            foreach (char x in text.ToCharArray())
            {
                if((int.TryParse(x.ToString(), out val)) || (x == '.'))
                {
                    result = true;
                }
                else
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        static string RunGitClone(string name, string path)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.RedirectStandardOutput = true;
                //p.StartInfo.FileName = Git;
                p.StartInfo.FileName = configItems["Git"];
                p.StartInfo.Arguments = $"clone -b {name} https://n8stansell2@bitbucket.org/veii/kiosk.git {path}";
                Output($"running git {p.StartInfo.Arguments}{newlines}");
                p.Start();

                p.WaitForExit();

                return $"{p.StandardError.ReadToEnd()}{p.StandardOutput.ReadToEnd()}";
            }
            catch (Exception ex)
            {
                return $"fatal: {ex.Message}";
            }
        }

        static string RestoreNuGetPackages(string path)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.RedirectStandardOutput = true;
                //p.StartInfo.FileName = NuGet;
                p.StartInfo.FileName = configItems["NuGet"];
                p.StartInfo.WorkingDirectory = path;
                p.StartInfo.Arguments = "restore";
                Output($"running nuget restore{newlines}");
                p.Start();

                p.WaitForExit();

                var error = p.StandardError.ReadToEnd();
                var success = p.StandardOutput.ReadToEnd();

                if (error.Length > 0) return $"fatal: {error}";
                else return success;
            }
            catch (Exception ex)
            {
                return $"fatal: {ex.Message}";
            }
        }

        static string BuildProject(string path)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.UseShellExecute = false;
                p.OutputDataReceived += BuildOutputDataReceived;
                p.EnableRaisingEvents = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.RedirectStandardOutput = true;
                //p.StartInfo.FileName = MSBuild;
                p.StartInfo.FileName = configItems["MSBuild"];
                p.StartInfo.WorkingDirectory = path;
                p.StartInfo.Arguments = "KioskUI.sln /t:build /p:configuration=release";
                Output($"running msbuild {p.StartInfo.Arguments}{newlines}");
                p.Start();
                p.BeginOutputReadLine();

                p.WaitForExit();

                var error = p.StandardError.ReadToEnd();
                var success = "OK";

                if (error.Length > 0) return $"fatal: {error}";
                else return success;
            }
            catch (Exception ex)
            {
                return $"fatal: {ex.Message}";
            }
        }

        static void BuildOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
            {
                if(!string.IsNullOrEmpty(e.Data))
                {
                    if(detailed)
                    {
                        Output($"{e.Data}{Environment.NewLine}");
                    }
                    else
                    {
                        log += $"{e.Data}{Environment.NewLine}";
                    }
                }
            }
        }

        static string RunInnoSetup(string outputPath)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.UseShellExecute = false;
                p.OutputDataReceived += InnoScriptDataReceived;
                p.EnableRaisingEvents = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.RedirectStandardOutput = true;
                //p.StartInfo.FileName = ISCC;
                p.StartInfo.FileName = configItems["ISCC"];
                p.StartInfo.WorkingDirectory = @"C:\Temp";
                p.StartInfo.Arguments = "/O\"" + outputPath + "\" C:\\Temp\\InnoScript.iss";
                Output($"running ISCC {p.StartInfo.Arguments}{newlines}");
                p.Start();
                p.BeginOutputReadLine();

                p.WaitForExit();

                var error = p.StandardError.ReadToEnd();
                var success = p.StandardOutput.ReadToEnd();

                if (error.Length > 0) return $"fatal: {error}";
                else return success;
            }
            catch (Exception ex)
            {
                return $"fatal: {ex.Message}";
            }
        }

        private static void InnoScriptDataReceived(object sender, DataReceivedEventArgs e)
        {
            using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
            {
                if (!string.IsNullOrEmpty(e.Data))
                {
                    if (detailed)
                    {
                        Output($"{e.Data}{Environment.NewLine}");
                    }
                    else
                    {
                        log += $"{e.Data}{Environment.NewLine}";
                    }
                }
            }
        }

        static string BuildScript()
        {
            List<InnoLine> defines = new List<InnoLine>();
            List<string> mainScript = new List<string>();

            // Handle definitions
            defines.Add(new InnoLine("; ", "Application Info"));
            defines.Add(new InnoLine("MainAppName", "VE Kiosk", true));
            defines.Add(new InnoLine("SetupPublisher", "Vendors Exchange International Inc.", true));
            defines.Add(new InnoLine("SetupURL", "https://www.veii.com", true));
            defines.Add(new InnoLine(string.Empty, string.Empty));
            defines.Add(new InnoLine("; ", "Source Release Directories"));
            //defines.Add(new InnoLine("IconSourcePath", IconSourcePath, true));
            //defines.Add(new InnoLine("FileSourcePath", FileSourcePath, true));
            defines.Add(new InnoLine("IconSourcePath", configItems["IconSourcePath"], true));
            defines.Add(new InnoLine("FileSourcePath", configItems["FileSourcePath"], true));
            defines.Add(new InnoLine("KioskRelease", RepoPath + KioskRelease, true));
            defines.Add(new InnoLine("KioskImages", RepoPath + KioskImages, true));
            defines.Add(new InnoLine("SetupRelease", RepoPath + SetupRelease, true));
            defines.Add(new InnoLine("TesterRelease", RepoPath + TesterRelease, true));
            defines.Add(new InnoLine("ShellRelease", RepoPath + ShellRelease, true));
            defines.Add(new InnoLine(string.Empty, string.Empty));
            defines.Add(new InnoLine("; ", "Installation Directories"));
            defines.Add(new InnoLine("IconInstallPath", IconInstallPath, true));
            defines.Add(new InnoLine("KioskReleaseInstallPath", KioskReleaseInstallPath, true));
            defines.Add(new InnoLine("KioskImagesInstallPath", KioskImagesInstallPath, true));
            defines.Add(new InnoLine("SetupInstallPath", SetupInstallPath, true));
            defines.Add(new InnoLine("TesterInstallPath", TesterInstallPath, true));
            defines.Add(new InnoLine("ShellInstallPath", ShellInstallPath, true));
            defines.Add(new InnoLine(string.Empty, string.Empty));
            defines.Add(new InnoLine("; ", "EXE Names"));
            defines.Add(new InnoLine("KioskExe", KioskExe, true));
            defines.Add(new InnoLine("SetupEXE", SetupEXE, true));
            defines.Add(new InnoLine("TesterExe", TesterExe, true));
            defines.Add(new InnoLine(string.Empty, string.Empty));
            defines.Add(new InnoLine("; ", "Icon Display Names"));
            defines.Add(new InnoLine("KioskExeName", KioskExeName, true));
            defines.Add(new InnoLine("SetupExeName", SetupExeName, true));
            defines.Add(new InnoLine("TesterExeName", TesterExeName, true));

            // Handle main script
            mainScript.Add("[Setup]");
            mainScript.Add("; NOTE: The value of AppId uniquely identifies this application.");
            mainScript.Add("; Do not use the same AppId value in installers for other applications.");
            mainScript.Add("; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)");
            mainScript.Add("AppId={{100E5826-86B5-45C9-B9B0-B9EE9F5EDE2E}");
            mainScript.Add("AppVersion=" + KioskUIVersion);
            mainScript.Add("AppName={#MainAppName}");
            mainScript.Add("AppPublisher={#SetupPublisher}");
            mainScript.Add("AppPublisherURL={#SetupURL}");
            mainScript.Add("AppSupportURL={#SetupURL}");
            mainScript.Add("AppUpdatesURL={#SetupURL}");
            mainScript.Add(@"DefaultDirName=C:\VEKioskUI");
            mainScript.Add("DisableDirPage=yes");
            mainScript.Add("DefaultGroupName=VE Kiosk");
            mainScript.Add("DisableProgramGroupPage=no");
            mainScript.Add($"OutputBaseFilename=VEKioskSetup_{BuildDate}");
            mainScript.Add("Compression=lzma");
            mainScript.Add("SolidCompression=yes");
            mainScript.Add(string.Empty);
            mainScript.Add("[CustomMessages]");
            mainScript.Add("CreateDesktopIcon=Select if you want setup to create a desktop icon");
            mainScript.Add(string.Empty);
            mainScript.Add("[Languages]");
            mainScript.Add("Name: \"english\"; MessagesFile: \"compiler:Default.isl\"");
            mainScript.Add(string.Empty);
            mainScript.Add("[Tasks]");
            mainScript.Add("Name: \"desktopicon\"; Description: \"{cm:CreateDesktopIcon}\"; GroupDescription: \"{cm:AdditionalIcons}\"; Flags: unchecked");
            mainScript.Add(string.Empty);
            mainScript.Add("[Files]");
            mainScript.Add("Source: \"{#IconSourcePath}\\*\"; DestDir: \"{app}\\{#IconInstallPath}\"; Flags: ignoreversion recursesubdirs createallsubdirs;");
            mainScript.Add("Source: \"{#FileSourcePath}\\*\"; DestDir: \"{app}\"; Flags: ignoreversion;");
            mainScript.Add("Source: \"{#KioskRelease}\\*\"; DestDir: \"{app}\\{#KioskReleaseInstallPath}\"; Flags: ignoreversion recursesubdirs createallsubdirs");
            mainScript.Add("Source: \"{#KioskImages}\\*\"; DestDir: \"{app}\\{#KioskImagesInstallPath}\"; Flags: ignoreversion recursesubdirs createallsubdirs");
            mainScript.Add("Source: \"{#SetupRelease}\\*\"; DestDir: \"{app}\\{#SetupInstallPath}\"; Flags: ignoreversion recursesubdirs createallsubdirs");
            mainScript.Add("Source: \"{#TesterRelease}\\*\"; DestDir: \"{app}\\{#TesterInstallPath}\"; Flags: ignoreversion recursesubdirs createallsubdirs");
            mainScript.Add("Source: \"{#ShellRelease}\\*\"; DestDir: \"{app}\\{#ShellInstallPath}\"; Flags: ignoreversion recursesubdirs createallsubdirs");
            mainScript.Add("; NOTE: Don't use \"Flags: ignoreversion\" on any shared system files");
            mainScript.Add(string.Empty);
            mainScript.Add("[Icons]");
            mainScript.Add("Name: \"{commondesktop}\\{#MainAppName}\"; Filename: \"{app}\\{#KioskReleaseInstallPath}\\{#KioskExe}\"; Tasks: desktopicon; IconFilename: \"{app}\\{#IconInstallPath}\\Kiosk.ico\"");
            mainScript.Add("Name: \"{group}\\{#MainAppName}\"; Filename: \"{app}\\{#KioskReleaseInstallPath}\\{#KioskExe}\"; IconFilename: \"{app}\\{#IconInstallPath}\\Kiosk.ico\"");
            mainScript.Add("Name: \"{app}\\{#MainAppName}\"; Filename: \"{app}\\{#KioskReleaseInstallPath}\\{#KioskExe}\"; IconFilename: \"{app}\\{#IconInstallPath}\\Kiosk.ico\"");
            mainScript.Add("Name: \"{group}\\{#SetupExeName}\"; Filename: \"{app}\\{#SetupInstallPath}\\{#SetupEXE}\"; IconFilename: \"{app}\\{#IconInstallPath}\\Setup.ico\"");
            mainScript.Add("Name: \"{app}\\{#SetupExeName}\"; Filename: \"{app}\\{#SetupInstallPath}\\{#SetupEXE}\"; IconFilename: \"{app}\\{#IconInstallPath}\\Setup.ico\"");
            mainScript.Add("Name: \"{group}\\{#TesterExeName}\"; Filename: \"{app}\\{#TesterInstallPath}\\{#TesterExe}\"; IconFilename: \"{app}\\{#IconInstallPath}\\Test.ico\"");
            mainScript.Add("Name: \"{app}\\{#TesterExeName}\"; Filename: \"{app}\\{#TesterInstallPath}\\{#TesterExe}\"; IconFilename: \"{app}\\{#IconInstallPath}\\Test.ico\"");
            mainScript.Add(string.Empty);
            mainScript.Add("[Run]");
            mainScript.Add("Filename: \"{app}\\{#SetupInstallPath}\\{#SetupEXE}\"; Description: \"{cm:LaunchProgram,{#StringChange(SetupExeName, '&', '&&')}}\"; Flags: nowait postinstall skipifsilent");

            var script = new StringBuilder();

            foreach(var x in defines)
            {
                if (x.KeyWord.Contains(";") || x.KeyWord == string.Empty) script.AppendLine($"{x.KeyWord}{x.Value}");
                else
                {
                    if (x.UseQuotes) script.AppendLine($"#define {x.KeyWord} {x.GetValueWithQuotes()}");
                    else script.AppendLine($"#define {x.KeyWord} {x.Value}");
                }
            }

            script.AppendLine(string.Empty);

            foreach(var x in mainScript)
            {
                script.AppendLine(x);
            }

            return script.ToString();
        }

        static void ExitApplication(int result, string log, string path)
        {
            try
            {
                File.WriteAllText($"{path}_{BuildDate}_log.txt", log);
                Environment.Exit(result);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                Environment.Exit(-1);
            }
        }
    }

    static class MsBuildHelper
    {
        public static BuildResult RunMsBuild(string projectFile, string target, IDictionary<string, string> properties)
        {
            BuildParameters parameters = new BuildParameters(new ProjectCollection())
            {
                Loggers = new ILogger[] { new ConsoleLogger() }
            };

            /*BuildParameters parameters = new BuildParameters(new ProjectCollection())
            {
                Loggers = new[] {new MySimpleLogger { } }
            };*/

            return BuildManager.DefaultBuildManager.Build(parameters, new BuildRequestData(projectFile, properties, null, new[] { target }, null));
        }
    }

    public class InnoLine
    {
        public string KeyWord { get; set; }
        public string Value { get; set; }
        public bool UseQuotes { get; set; }

        public InnoLine(string keyword, string value)
        {
            KeyWord = keyword;
            Value = value;
            UseQuotes = false;
        }

        public InnoLine(string keyword, string value, bool useQuotes)
        {
            KeyWord = keyword;
            Value = value;
            UseQuotes = useQuotes;
        }

        public string GetValueWithQuotes()
        {
            return "\"" + Value + "\"";
        }
    }
}
