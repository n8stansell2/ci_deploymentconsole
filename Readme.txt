﻿Syntax: deploymentconsole.exe [repo name] [detailed console]

 Example: deploymentconsole.exe VEK-649-post-script-for-ci-builder T
 
 Description:         What it tries to do:
                      1. download repo using Git Clone (Assumes Git is logged in!)
                      2. run NuGet.exe to restore any NuGet Packages that may be required
                      3. run MSBuild to compile project release
                      4. create script file for InnoScript
                      5. build setup file

                      NOTES:  any repo will be copied to C:\VEKioskRepos\<repo name>
                              a log file will be created as C:\VEKioskRepos\<repo name>_date_time_log.txt
                              the setup file will be created as C:\FTP\VEKioskReleases\<repo name>\VEKioskSetup_date_time.exe

Parameters:
  [repo name]         should be the name of the Git repo
  
  [detailed console]  set to "T" or "True" to view all activity in the console
  
Requirements
                      There are supporting directories that must be present
                          1. C:\VEKioskStaging - Any files you wish to be included in the C:\VEKioskUI install folder, place here
                          2. C:\VEKioskStaging\Icons - Icons required for file links
                              a. The icons required to be present in this directory are "KioskUI.ico", "Setup.ico", and "Test.ico".
                                 As long as these files exist they can be whatever images you like.
                      
                      There are supporting applications that must be installed and their directories defined
                          1. Git - Default path is "C:\Program Files\Git\bin\git.exe"
                          2. Nuget - Default path is ""C:\VEKioskUIStaging\NuGet\nuget.exe""
                          3. MSBuild - Default path is "C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe"
                          4. InnoScript - Default path is "C:\Program Files (x86)\Inno Setup 5\ISCC.exe"
						  
						  NOTE: These directory paths are availiable in the application configuration file